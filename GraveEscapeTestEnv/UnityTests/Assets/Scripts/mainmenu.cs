﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainmenu : MonoBehaviour
{

    //make sure in build settings that main menu is at 0 and game is at 1 
    public void PlayGame ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Time.timeScale = 1f;
    }
    //this will only show working when built. debug is to show it is working when debugging in unity
    public void QuitGame ()
    {
        Debug.Log("quit");
        Application.Quit();
    }

  
}
