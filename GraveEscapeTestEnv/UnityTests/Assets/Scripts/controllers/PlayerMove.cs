﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    [SerializeField] private string forwardInputName;
    [SerializeField] private string sidestepInputName;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private KeyCode runKey;


    private CharacterController charController;
    private bool isRunning;

    //running functions
    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private AnimationCurve jumpFalloff;

    private bool isJumping;

    // jitter / bounce removal
    [SerializeField] private float slopeForceRayLength;
    [SerializeField] private float slopeForce;
    

    private void Awake()
    {
        charController = GetComponent <CharacterController>();
        isRunning = false;
    }

    bool onSlope()
    {
        if (isJumping)
        {
            return false;
        }

        RaycastHit groundHit;

        if (Physics.Raycast(transform.position, //where to start ray cast
            Vector3.down, //direction of ray
            out groundHit, //return info about ray-object hit / iontersection
            charController.height / 2.0f * slopeForceRayLength // length of ray
            ))
        {
            if (groundHit.normal != Vector3.up)
            {
                return true;
            }
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {      
    }

    // Update is called once per f0rame
    void Update()
    {
        CheckRunKey();
        PlayerMovement();
    }

    void CheckRunKey()
    {
        if (Input.GetKeyDown(runKey))
        {
            isRunning = true;
        }
        else if (Input.GetKeyUp(runKey))
        {
            isRunning = false;
        }
    }

    void PlayerMovement()
    {
        float actualSpeed;

        if (isRunning)
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = moveSpeed; //walk speed
        }


        float forwardInput = Input.GetAxis(forwardInputName);
        float sidestepInput = Input.GetAxis(sidestepInputName);

        Vector3 forwardMovement = transform.forward * forwardInput;
        Vector3 sideMovement = transform.right * sidestepInput;


        Vector3 actualMovement = Vector3.ClampMagnitude(forwardMovement + sideMovement, 1.0f) * actualSpeed;
        charController.SimpleMove(actualMovement);

        //Handle Jump
        JumpInput();

        // resolve bounce on slope
        if ((forwardInput != 0 || sidestepInput != 0) && onSlope())
        {
            Vector3 downForce = Vector3.down * (charController.height / 2.0f) * slopeForce * Time.deltaTime;
            charController.Move(downForce);
        }
    }

    void JumpInput()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    IEnumerator JumpEvent()
    {
        float timeInAir = 0;
        charController.slopeLimit = 90.0f;

        do
        {
            float jumpForce = jumpFalloff.Evaluate(timeInAir);

            charController.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;
        }
        while (

            !charController.isGrounded &&
            charController.collisionFlags != CollisionFlags.Above
        );

        charController.slopeLimit = 45.0f;
        isJumping = false;
    }
}
